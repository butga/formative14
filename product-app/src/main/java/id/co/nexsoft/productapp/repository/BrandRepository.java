package id.co.nexsoft.productapp.repository;

import id.co.nexsoft.productapp.entity.Brand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Integer> {
    Brand findById(int id);
    List<Brand> findAll();
}
