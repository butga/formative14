create database formative14;

create table brand(
	id int not null auto_increment,
    name varchar(255),
    primary key(id)
);

create table product(
	id int not null auto_increment,
    art_number varchar(50),
    name varchar(50),
    description varchar(150),
    brand_id int,
    primary key(id)
);

insert into brand(name)
	values("Eiger"),("Rei"),("Kalibre");
    
insert into product(art_number, name, description, brand_id)
	values("REI-01", "REI 1", "TAS OUTDOR", 2),
		("REI-02", "REI 2", "TAS kantor", 2),
        ("EIGER-01", "EIGER 1", "TAS OUTDOR", 1),
        ("KALIBRE-01", "KALIBRE 1", "TAS OUTDOR", 3),
        ("KALIBRE-02", "KALIBRE 2", "TAS KANTOR", 3);
    
    